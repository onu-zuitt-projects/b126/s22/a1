//Inside of b126-chatapp, do the following:

/*
	1. Create a channels collection and inside of that, create 5 different documents each with the following information:
		-"channelName": ("Red Team", "Blue Team", "Yellow Team", "Black Team", "White Team")
		-"members": Any 3 names as strings

		db.channels.insertMany([
	{
		"channelName": "Red Team",
		"members": {"Ada", "Emeka", "Sunny"}
	},
	{
		"channelName": "Blue Team",
		"members":{ "Biko", "Nnna", "Okilo" }
	
	},	
	{
		"channelName": "Yellow Team",
		"members":{ "Mercy", "Nunu", "Brass" }
	
	},
	{
		"channelName": "Black Team",
		"members":{ "Victor", "Ini", "Amaka" }
	
	},		
	{
		"channelName": "White Team",
		"members":{ "Jenny", "Obasi", "Basil" }
		
	},	
	])
	

	2. Add an "isActive" property to all channels with a value of true (boolean)
	db.channels.updateMany(
		{},
		{
			$set: {
				"isActive": true
			}
		}
		
	)

	3. Change the Yellow and Black Team to inactive. 

	db.channels.updateMany(
		{
			"_id": 0
		},
		{
			$set: {
				"isAvailable": false
			}
		}
		
	)

	4. Remove the White Team channel

	5. Find all active channels

	6. Delete all inactive channels 

*/